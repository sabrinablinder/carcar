# Wardrobify

Team:

* Sonny Nguyen - Hats microservice
* Person 2 - Which microservice?

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

Hats microservice components:
    - Poller that fetches location-data from wardrobe API
    - RESTfulized API:
        - Display list of hats
        - Create a new hat
        - Delete hat
        - Update hat
        - Show Hat details
    - React Frontend:
        - List hats in a table
        - View each hat detail
        - Form to create a new hat
        - Button to delete hat
        - Routs for all interactions with the hat object
