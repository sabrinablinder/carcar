import React, {useEffect, useState} from "react";
import { useParams, useNavigate } from "react-router-dom";

function HatDetails() {
    const navigate = useNavigate()
    const [hat, setHat] = useState('')
    let params = useParams()
    const id = params.id
    const fetchData = async () => {
        const url = `http://localhost:8090/api/hats/${id}/`
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setHat(data.hat)
        }
    }

    useEffect(() => {fetchData()}, [])

    if (!hat) {
        return <p>Hat not found</p>;
      }

    const locationName = hat.location ? hat.location.location_name : '';


    const clickHandler = async event => {
        const url = `http://localhost:8090/api/hats/${id}/`
        const fetchConfig = {
            method:'delete',
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const response = await fetch(url, fetchConfig)
        if (response.ok){
            navigate('/hats')
        }
    }

    return (
        <div className='container-fluid' style={{'textAlign': 'center'}}>
            <img src={hat.picture_url} className="img-thumbnail" alt={hat.style_name} style={{'maxWidth': '500px', 'margin':'20px'}}></img>
            <h2>{hat.style_name}</h2>
            <h4 style={{'marginTop': '20px'}}>{hat.color}</h4>
            <h4>{hat.fabric}</h4>
            <h6 style={{'marginTop': '20px'}}>{locationName}</h6>
            <button className="btn btn-danger" style={{'marginTop': '10px'}} onClick={clickHandler}>Remove</button>


        </div>
    )
}

export default HatDetails
