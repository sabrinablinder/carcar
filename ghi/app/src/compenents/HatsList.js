import React, {useEffect, useState} from "react";
import { NavLink, Link } from "react-router-dom";

function HatsList() {
    const [hats,setHats] = useState([])
    const fetchData = async () => {
        const response = await fetch('http://localhost:8090/api/hats/')
        if (response.ok){
            const data = await response.json()
            setHats(data.hats)
        }
    }

    useEffect(()=>{fetchData()}, [])

    return(
        <React.Fragment>

        <div className="container-fluid">
            <h1>Hats</h1>
            <NavLink className='btn btn-primary' to="/hats/new">New Hat</NavLink>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Style</th>
                        <th>Color</th>
                        <th>Fabric</th>
                        <th>Location</th>
                    </tr>

                </thead>
                <tbody>
                    {hats.map(hat=> {
                        return (
                            <tr key={hat.id}>
                                <td>
                                    <Link to={`${hat.id}`}>
                                        {hat.style_name}
                                    </Link>
                                </td>
                                <td>{hat.color}</td>
                                <td>{hat.fabric}</td>
                                <td>{hat.location.location_name}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
        </React.Fragment>
    )
}

export default HatsList
