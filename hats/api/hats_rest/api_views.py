from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import LocationVO, Hat

class LocationVOListEncoder(ModelEncoder):
    model = LocationVO
    properties = ["import_href", "location_name"]


class HatsListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "style_name",
        "id",
        "fabric",
        "color",
        "picture_url",
        "location"
    ]
    encoders={"location": LocationVOListEncoder()}




#for self visualization of the current VO Models
@require_http_methods(["GET"])
def api_show_locationVOs(request):
    if request.method == "GET":
        locations = LocationVO.objects.all()
        return JsonResponse(
            {"locations": locations},
            LocationVOListEncoder
        )

#Create new hat or display list of hats
@require_http_methods(['GET', "POST"])
def api_list_hats(request):
    if request.method == 'GET':
        hats = Hat.objects.all()
        return JsonResponse({'hats': hats}, HatsListEncoder)
    else:
        content = json.loads(request.body)
        try:
            location = LocationVO.objects.get(import_href=content["location"])
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Location ID"}
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatsListEncoder,
            safe=False
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_hats(request, pk):
    if request.method == 'GET':
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            {'hat': hat},
            encoder=HatsListEncoder,
            safe=False
        )

    elif request.method == "PUT":
        hat = Hat.objects.get(id=pk)
        content = json.loads(request.body)
        try:
            if "location" in content:
                location = LocationVO.objects.get(import_href=content["location"])
                content["location"] = location
        except:
            return JsonResponse(
                {"message": "Invalid Location ID"}, status=400
            )
        Hat.objects.filter(id=pk).update(**content)
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatsListEncoder,
            safe=False
        )

    else:
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
