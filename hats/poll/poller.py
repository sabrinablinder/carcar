import django
import os
import sys
import time
import json
import requests

sys.path.append("")
sys.path.append("/app/hats/api")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()

from hats_rest.models import LocationVO

def get_locations():
    response = requests.get("http://wardrobe-api:8000/api/locations/")
    content = json.loads(response.content)
    for location in content["locations"]:
        LocationVO.objects.update_or_create(
            import_href=location["href"],
            defaults={
            "location_name": f"{location['closet_name']}, Section: {location['section_number']}, Shelf: {location['shelf_number']}"
            }
        )

def poll():
    while True:
        try:
            get_locations()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
